// To see the Bug: 
//  select starting from anywhere after the beginning { of subFunction()
//  then drag to the beginning { of subFunction()

// You will notice the selection will highlight the opening {, 
//   however if you copy and past the selected text, it will NOT contain {

// You will also notice this doe NOT happen when attempting the same process on containingFunction()
//  but it will happen on anotherFunction() 
//  This seems to be a result of only some next-line content triggering this issue and other content doesn't. 
//  If there is an empty line after, for example, this issue doesn't not occur

function containingFunction(){
    function subFunction(){
        console.log("Some Output");
    }
    // Issue is not present when the line after the function name and opening { is empty
    function subFunction(){

        console.log("OPutput");
    }
}

function anotherFunction(){
    console.log("Other Output");
}
